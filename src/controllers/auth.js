const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const sgMail = require('@sendgrid/mail');

const { User, UserJoiSchema } = require('../models/User');
const passwordGenerator = require('../helpers/passwordGenerator');

const register = async (req, res) => {
  const { email, password, role } = req.body;

  await UserJoiSchema.validateAsync({ email, password, role });

  const user = new User({
    email,
    password: await bcrypt.hash(password, bcrypt.hashSync(password, bcrypt.genSaltSync(12))),
    role,
  });

  await user.save();

  return res.status(200).json({ message: 'Profile created successfully' });
};

const login = async (req, res) => {
  const { email, password } = req.body;

  await UserJoiSchema.validateAsync({ email, password });

  const user = await User.findOne({ email });

  const correctPass = await bcrypt.compare(String(password), String(user.password));

  if (correctPass && user) {
    const payload = {
      userId: user._id,
      email: user.email,
      role: user.role,
      created_date: user.created_date,
    };
    const jwtToken = jwt.sign(payload, process.env.JWT_SECRET, {
      expiresIn: process.env.JWT_LIFETIME,
    });

    return res.status(200).json({
      jwt_token: jwtToken,
    });
  }
};

const forgotPassword = async (req, res) => {
  const { email } = req.body;

  await UserJoiSchema.validateAsync({ email });

  try {
    const user = await User.findOne({ email });

    if (!user) {
      return res.status(400).json({
        message: "There isn't such user",
      });
    }

    const password = passwordGenerator();

    const encryptedPassword = bcrypt.hashSync(
      password,
      bcrypt.genSaltSync(12),
    );

    user.password = encryptedPassword;

    await user.save();


    sgMail.setApiKey(process.env.API_KEY);

    const message = {
      to: email,
      from: 'artemix.portfolio@gmail.com',
      subject: 'Reset password',
      text: `Your new password is ${password}`,
    };

    sgMail.send(message)
      .then((response) => console.log(response))
      .catch((e) => console.log(e.message));

    return res.status(200).json({ message: 'New password sent to your email address' });
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};

module.exports = { register, login, forgotPassword };
