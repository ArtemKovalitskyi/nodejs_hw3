/* eslint-disable camelcase */

const { Load, LoadJoiSchema } = require('../models/Load');
const { Truck } = require('../models/Truck');

const getTruckType = require('../helpers/getTruckType');

const { loadStatus, loadStates } = require('../constants');

const getLoads = async (req, res) => {
  const { role, userId } = req.user;

  let loads = null;

  try {
    if (role === 'DRIVER') {
      loads = await Load.find(
        {
          $or: [
            { assigned_to: userId },
            { status: ['ASSIGNED', 'SHIPPED'] },
          ],
        },
        '-__v -logs._id',
      );
    }

    if (role === 'SHIPPER') {
      loads = await Load.find({ created_by: userId }, '-__v -logs._id');
    }

    if (loads.length === 0) {
      return res.status(400).json({ message: `Loads for a user with id: ${userId} is not found` });
    }

    return res.status(200).json({ loads });
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};

const addLoad = async (req, res) => {
  const {
    name, payload, pickup_address, delivery_address, dimensions,
  } = req.body;
  const { role, userId } = req.user;

  await LoadJoiSchema.validateAsync({
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  });

  if (role === 'SHIPPER') {
    const load = new Load({
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
      created_by: userId,
    });

    await load.save();

    return res.status(200).json({ message: 'Load created successfully' });
  }
};

const getActiveLoad = async (req, res) => {
  const { userId, role } = req.user;
  try {
    if (role === 'DRIVER') {
      const activeLoad = await Load
        .findOne(
          { assigned_to: userId, status: loadStatus.ASSIGNED },
          { __v: false },
        );

      return res.status(200).json({ load: activeLoad });
    }

    return res.status(400).json({ message: 'You are not a driver' });
  } catch (e) {
    return res.status(400).json({ message: e });
  }
};

const setNextLoadState = async (req, res) => {
  const { userId, role } = req.user;
  if (role === 'DRIVER') {
    const changeLoad = await Load.findOne({
      assigned_to: userId,
      status: loadStatus.ASSIGNED,
    });
    if (!changeLoad) return res.status(400).json({ message: 'Load was not found' });

    const newState = loadStates[loadStates.indexOf(changeLoad.state) + 1];

    if (!newState) return res.status(400).json({ message: `Load already has ${changeLoad.state} state` });

    if (newState === loadStates[3]) {
      changeLoad.status = loadStatus.SHIPPED;
    }

    changeLoad.state = newState;
    await changeLoad.save();

    return res.status(200).json({ message: `Load state changed to '${changeLoad.state}'` });
  }
};

const getLoadById = async (req, res) => {
  const { id } = req.params;
  const { userId } = req.user;

  try {
    const load = await Load.findOne({ _id: id, created_by: userId });

    return res.status(200).json({ load });
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};

const updateLoadById = async (req, res) => {
  const { id } = req.params;
  const { userId } = req.user;

  try {
    await Load.findOneAndUpdate({
      _id: id,
      created_by: userId,
      status: 'NEW',
    }, req.body, { new: true });

    return res.status(200).json({ message: 'Load details changed successfully' });
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};

const deleteLoadById = async (req, res) => {
  const { id } = req.params;
  const { userId, role } = req.user;

  try {
    if (role === 'SHIPPER') {
      await Load.findOneAndDelete({ _id: id, created_by: userId });

      return res.status(200).json({ message: 'Load deleted successfully' });
    }

    return res.status(400).json({ message: 'You are not a shipper' });
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};

const postLoadById = async (req, res) => {
  const { id } = req.params;
  const { role, userId } = req.user;

  try {
    if (role === 'SHIPPER') {
      const load = await Load.findOne({ _id: id, created_by: userId });

      const truckType = getTruckType({ dimensions: load.dimensions, payload: load.payload });

      const truck = await Truck.findOne({
        status: 'IS',
        assigned_to: { $exists: true },
        type: truckType,
      });

      if (!truck) return res.status(400).json({ message: 'There is no available truck' });

      truck.status = 'OL';
      await truck.save();

      load.status = loadStatus.ASSIGNED;
      load.state = loadStates[0];
      load.assigned_to = truck.assigned_to;
      load.logs.push({
        message: `Load was assigned to driver with id: ${truck.assigned_to}`,
      });
      await load.save();

      return res.status(200).json({
        message: 'Load posted successfully',
        driver_found: true,
      });
    }

    return res.status(400).json({ message: 'You are not a shipper' });
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};

const getLoadShippingInfoById = async (req, res) => {
  const { id } = req.params;
  const { userId, role } = req.user;

  try {
    if (role === 'SHIPPER') {
      const load = await Load.findOne({ _id: id, created_by: userId }, { __v: false });

      if (load.status !== loadStatus.ASSIGNED) {
        return {
          message: `Load has status: ${load.status}`,
        };
      }

      const truck = await Load.findOne({ assigned_to: load.assigned_to }, { __v: false });

      return res.status(200).json({ load, truck });
    }

    return res.status(400).json({ message: 'You are not a shipper' });
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};

module.exports = {
  getLoads,
  addLoad,
  getActiveLoad,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
  getLoadShippingInfoById,
  setNextLoadState,
};
