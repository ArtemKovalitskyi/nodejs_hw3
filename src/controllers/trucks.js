const { Truck, TruckJoiSchema } = require('../models/Truck');

const getTrucks = async (req, res) => {
  const { userId, role } = req.user;

  try {
    const userTrucks = await Truck.find(
      { created_by: userId },
      '_id created_by assigned_to type status created_date',
    );

    if (userTrucks.length === 0) {
      return res.status(400).json({ message: 'Trucks are not yet created' });
    }

    if (userTrucks && role === 'DRIVER') return res.status(200).json({ trucks: userTrucks });

    return res.status(400).json({ message: 'You are not a Driver' });
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};

const addTruck = async (req, res) => {
  const { type } = req.body;
  const { role } = req.user;

  if (role === 'DRIVER') {
    const truck = new Truck({
      created_by: req.user.userId,
      type,
      status: 'IS',

    });

    await truck.save();

    return res.status(200).json({ message: 'Truck created successfully' });
  }

  return res.status(400).json({ message: 'You are not a Driver' });
};

const getTruckById = async (req, res) => {
  const { id } = req.params;
  const { role, userId } = req.user;

  try {
    if (role === 'DRIVER') {
      const truck = await Truck.findOne({ _id: id, created_by: userId });

      return res.status(200).json({ truck });
    }
    return res.status(400).json({ message: 'You are not a Driver' });
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};

const updateTruck = async (req, res) => {
  const { id } = req.params;
  const { type } = req.body;
  const { role, userId } = req.user;

  await TruckJoiSchema.validateAsync({ type });

  try {
    if (role === 'DRIVER') {
      await Truck.findOneAndUpdate({ _id: id, created_by: userId }, { type }, { new: true });

      return res.status(200).json({ message: 'Truck details changed successfully' });
    }

    return res.status(400).json({ message: 'You are not a Driver' });
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};

const deleteTruck = async (req, res) => {
  const { id, userId } = req.params;
  const { role } = req.user;

  try {
    if (role === 'DRIVER') {
      await Truck.findOneAndDelete({ _id: id, created_by: userId });

      return res.status(200).json({ message: 'Truck deleted successfully' });
    }

    return res.status(400).json({ message: 'You are not a Driver' });
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};

const assignTruckToUser = async (req, res) => {
  const { id } = req.params;
  const { userId, role, email } = req.user;

  try {
    if (role === 'DRIVER') {
      const truckToAssign = await Truck.findById(id);

      if (truckToAssign.assigned_to.equals(userId)) {
        return res
          .status(400)
          .json({ message: `The truck has been assigned by ${email} before` });
      }

      const assignedTruckForUser = await Truck.findOne({ assigned_to: userId });

      if (assignedTruckForUser) {
        return res.status(400).json({
          message: `The driver ${email} can't assign more then one truck`,
        });
      }

      truckToAssign.assigned_to = userId;

      await truckToAssign.save();

      console.log(truckToAssign);

      return res.status(200).json({
        message: 'Truck assigned successfully',
      });
    }
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};

module.exports = {
  getTrucks, assignTruckToUser, addTruck, deleteTruck, getTruckById, updateTruck,
};
