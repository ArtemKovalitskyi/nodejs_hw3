const bcrypt = require('bcryptjs');
const { User } = require('../models/User');

const getUser = (req, res) => {
  const currentUser = req.user;

  try {
    return res.status(200).json({ user: currentUser });
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};

const deleteUser = async (req, res) => {
  const { userId } = req.user;

  try {
    await User.findByIdAndDelete(userId);

    return res.status(200).json({ message: 'Profile deleted successfully' });
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};

const changePassword = async (req, res) => {
  const { oldPassword, newPassword } = req.body;
  const { userId } = req.user;

  try {
    const user = await User.findOne({ _id: userId });

    if (await bcrypt.compare(String(oldPassword), String(user.password))) {
      user.password = await bcrypt.hash(newPassword, 12);

      await user.save();

      return res.status(200).json({ message: 'Password changed successfully' });
    }
    return res.status(400).json({ message: 'Password does not  match old pass' });
  } catch (e) {
    return res.status(400).json({ message: 'Password is invalid' });
  }
};

module.exports = {
  getUser, deleteUser, changePassword,
};
