const { TRUCK_TYPES } = require('../constants');

const getTruckType = ({ dimensions, payload }) => {
  // eslint-disable-next-line no-restricted-syntax
  for (const type in TRUCK_TYPES) {
    const typeStats = TRUCK_TYPES[type];
    if (
      typeStats.payload >= Number(payload)
      && typeStats.width >= Number(dimensions.width)
      && typeStats.height >= Number(dimensions.height)
      && typeStats.length >= Number(dimensions.length)
    ) return type;
  }
};

module.exports = getTruckType;
