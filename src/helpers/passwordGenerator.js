const genPassword = () => {
  const symbols = '0123456789abcdefghijklmnopqrstuvwxyz!@#$%^&*()ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  const lenPass = 8;
  let password = '';

  for (let i = 0; i <= lenPass; i++) {
    const randomNumber = Math.floor(Math.random() * symbols.length);
    password += symbols.substring(randomNumber, randomNumber + 1);
  }

  return password;
};

module.exports = genPassword;
