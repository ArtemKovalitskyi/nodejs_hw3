require('dotenv').config();

const express = require('express');

const app = express();
const morgan = require('morgan');

// connect DB
const connectDB = require('./db/connect');

//middlewares
const authMiddleware = require('./middleware/authMiddleware');

// routers
const authRouter = require('./routes/auth');
const userRouter = require('./routes/users');
const truckRouter = require('./routes/trucks');
const loadRouter = require('./routes/loads');

app.use(express.json());
app.use(morgan('tiny'));

// routes
app.use('/api/auth', authRouter);
app.use('/api/users/me', authMiddleware, userRouter);
app.use('/api/trucks', authMiddleware, truckRouter);
app.use('/api/loads', authMiddleware, loadRouter);

const port = process.env.PORT || 8080;

const start = async () => {
  try {
    await connectDB(process.env.MONGO_URI);
    app.listen(port, () => {
      console.log(`Server is listening on port ${port}...`);
    });
  } catch (e) {
    console.log(e);
  }
};

start();

// ERROR HANDLER
app.use(errorHandler);

/**
 *
 * @param {Error} err
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 */
function errorHandler(err, req, res, next) {
  console.log(err.message);
  res.status(500).send({ message: 'Server error' });

  next();
}
