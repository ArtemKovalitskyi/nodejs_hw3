const jwt = require('jsonwebtoken');
const { User } = require('../models/User');

const auth = async (req, res, next) => {
  const authHeader = req.headers.authorization;

  if (!authHeader && authHeader.startsWith('Bearer')) {
    return res.status(400).json({
      message: 'Authentication invalid',
    });
  }

  const token = authHeader.split(' ')[1];

  if (!token) {
    return res.status(400).json({
      message: 'Token for \'authorization\' is empty',
    });
  }

  try {
    const payload = jwt.verify(token, process.env.JWT_SECRET, {
      expiresIn: process.env.JWT_LIFETIME,
    });

    const findUser = await User.findById(payload.userId);

    if (!findUser) {
      return res.status(400).json({ message: 'User profile is not founded' });
    }

    req.user = payload;
    next();
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};

module.exports = auth;
