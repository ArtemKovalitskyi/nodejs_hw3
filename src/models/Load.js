const mongoose = require('mongoose');
const Joi = require('joi');

const { loadStates, loadStatus } = require('../constants');

// eslint-disable-next-line no-unused-vars
const LoadJoiSchema = Joi.object().keys({
  status: Joi.string()
    .custom((status, helper) => (loadStatus.includes(status)
      ? status
      : helper.message(
        `Status must be defined as some of these values: ${loadStatus.join(
          ', ',
        )}`,
      )))
    .optional(),
  state: Joi.string()
    .custom((state, helper) => (loadStates.includes(state)
      ? state
      : helper.message(
        `State must be defined as some of these values: ${loadStates.join(
          ', ',
        )}`,
      )))
    .optional(),

  name: Joi.string().min(2).required(),
  payload: Joi.number().required(),
  pickup_address: Joi.string().required(),
  delivery_address: Joi.string().required(),
  dimensions: Joi.object({
    width: Joi.number().required(),
    length: Joi.number().required(),
    height: Joi.number().required(),
  }).required(),
});

const LoadSchema = new mongoose.Schema({
  created_by: {
    type: mongoose.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Types.ObjectId,
  },
  status: {
    type: String,
    enum: Object.values(loadStatus),
    default: 'NEW',
    required: true,
  },
  state: {
    type: String,
    enum: Object.values(loadStates),
    default: 'En route to Pick Up',
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: [
    {
      message: {
        type: String,
        required: true,
      },
      time: {
        type: String,
        default: new Date().toISOString(),
        required: true,
      },
    },
  ],
  created_date: {
    type: String,
    default: new Date().toISOString(),
    required: true,
  },

});

const Load = mongoose.model('Load', LoadSchema);
module.exports = {
  Load,
  LoadJoiSchema,
};
