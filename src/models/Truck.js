const mongoose = require('mongoose');
const Joi = require('joi');

const TruckJoiSchema = Joi.object().keys({
  type: Joi.string()
    .regex(/SPRINTER|SMALL STRAIGHT|LARGE STRAIGHT/)
    .required(),
});

const TruckSchema = new mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Types.ObjectId,
    default: mongoose.Types.ObjectId,
    required: true,
    turnOn: true,

  },
  type: {
    type: String,
    required: true,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
  },
  status: {
    type: String,
    required: true,
    enum: ['OL', 'IS'],
  },
  created_date: {
    type: String,
    default: new Date().toISOString(),
    required: true,
  },

});

const Truck = mongoose.model('Truck', TruckSchema);

module.exports = {
  Truck,
  TruckJoiSchema,
};
