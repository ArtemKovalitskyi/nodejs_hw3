const mongoose = require('mongoose');
const Joi = require('joi');

const { ROLES } = require('../constants');

const UserJoiSchema = Joi.object().keys({
  email: Joi.string().email().required(),
  password: Joi.string()
    .pattern(/^[a-zA-Z0-9]/)
    .optional(),
  role: Joi.string()
    .regex(/DRIVER|SHIPPER/)
    .optional(),
});

const User = mongoose.model('User', {
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
    enum: ROLES,
  },
  created_date: {
    type: String,
    default: new Date().toISOString(),
    required: true,
  },
});

module.exports = {
  User,
  UserJoiSchema,
};
