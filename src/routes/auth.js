const { Router } = require('express');

const router = Router();

const { register, login, forgotPassword } = require('../controllers/auth');

router.post('/register', register);
router.post('/login', login);
router.post('/forgot_password', forgotPassword);

module.exports = router;
