const { Router } = require('express');

const router = Router();

const {
  getLoads,
  addLoad,
  deleteLoadById,
  getActiveLoad,
  getLoadById,
  getLoadShippingInfoById,
  postLoadById,
  setNextLoadState,
  updateLoadById,
} = require('../controllers/loads');

router.route('/').get(getLoads).post(addLoad);
router.get('/active', getActiveLoad);
router.post('/:id/post', postLoadById);
router.get('/:id/shipping_info', getLoadShippingInfoById);
router.route('/:id').get(getLoadById).put(updateLoadById).delete(deleteLoadById);
router.patch('/active/state', setNextLoadState);


module.exports = router;
