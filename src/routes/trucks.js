const { Router } = require('express');

const router = Router();

const {
  getTrucks, updateTruck, getTruckById, deleteTruck, addTruck, assignTruckToUser,
} = require('../controllers/trucks');

router.route('/').get(getTrucks).post(addTruck);
router.route('/:id').get(getTruckById).put(updateTruck).delete(deleteTruck);
router.post('/:id/assign', assignTruckToUser);

module.exports = router;
