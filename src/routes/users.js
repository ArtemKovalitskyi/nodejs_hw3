const { Router } = require('express');

const router = Router();

const { getUser, deleteUser, changePassword } = require('../controllers/users');

router.route('/').get(getUser).delete(deleteUser);
router.patch('/password', changePassword);

module.exports = router;
