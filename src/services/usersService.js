const bcryptjs = require('bcryptjs');
const { User } = require('../models/User');

const saveUser = async ({ name, username, password }) => {
  const user = new User({
    name,
    username,
    password: await bcryptjs.hash(password, 10),
  });

  return await user.save();
};

module.exports = {
  saveUser,
};
